xquery version "3.1";

(:
 : Copyright (c) 2018 Uwe Sikora
 : Copyright (c) 2018–2019 Michelle Weidling
 : Copyright (c) 2020 Stefan Hynek
 :
 : This file is part of intermediate-format.
 :
 : intermediate-format is free software: you can redistribute it and/or modify
 : it under the terms of the GNU General Public License as published by
 : the Free Software Foundation, either version 3 of the License, or
 : (at your option) any later version.
 :
 : intermediate-format is distributed in the hope that it will be useful,
 : but WITHOUT ANY WARRANTY; without even the implied warranty of
 : MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 : GNU General Public License for more details.
 :
 : You should have received a copy of the GNU General Public License
 : along with intermediate-format.  If not, see <https://www.gnu.org/licenses/>.
 :)

(:~
 : ifweb.xql offers a simple interface for creating the website specific BdN
 : intermediate format of a given resource.
 :
 : @author Michelle Rodzis
 : @version 1.0
 :
 :)

module namespace ifweb="http://bdn-edition.de/intermediate_format/ifweb";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

import module namespace console="http://exist-db.org/xquery/console";
import module namespace pre="http://bdn-edition.de/intermediate_format/preprocessing" at "preprocessing.xqm";
import module namespace ident = "http://bdn-edition.de/intermediate_format/identification" at "identification.xqm";
import module namespace config = "http://bdn-edition.de/intermediate_format/config" at "../config.xqm";

declare option exist:serialize "method=xml media-type=text/xml omit-xml-declaration=no indent=no";

declare variable $ifweb:replace-whitespace := true();

declare function ifweb:main($resource as xs:string) as xs:string? {
    let $doc := doc($config:sade-data || $resource)
    let $filename := substring-before($resource, '.xml') || "-if.xml"

    return ifweb:transform($doc, $filename)
};


declare function ifweb:transform($doc as node()*, $filename as xs:string)
as node()* {
    let $preprocessed-data := pre:preprocessing($doc/tei:TEI, $ifweb:replace-whitespace)
    let $intermediate-format := ident:walk($preprocessed-data, ())
    let $store := xmldb:store($config:sade-data, $filename, $intermediate-format)

    return $intermediate-format
};


declare function ifweb:transform($doc as node()*, $filename as xs:string,
$target-dir as xs:string) as node()* {
    let $preprocessed-data := pre:preprocessing($doc/tei:TEI, $ifweb:replace-whitespace)
    let $intermediate-format := ident:walk($preprocessed-data, ())
    let $store := xmldb:store($target-dir, $filename, $intermediate-format)

    return $intermediate-format
};


declare function ifweb:complete-xml($author as xs:string, $xml as node()*)
as node()* {
    let $filename := $author || "-full-if.xml"
    return ifweb:transform($xml, $filename)
};

declare function ifweb:single-xml($filename as xs:string, $src-dir as xs:string,
$target-dir as xs:string) as node()* {
    let $resource := doc($src-dir || $filename)
    return ifweb:transform($resource, $filename, $target-dir)
};

declare function ifweb:transform-split-xml($xml as node()*, $filename as xs:string,
$dir as xs:string) {
    let $preprocessed-data := pre:preprocessing($xml/*, $ifweb:replace-whitespace)
    let $editorial-notes := pre:get-editorial-notes($xml/*, $ifweb:replace-whitespace)
    let $intermediate-format := ident:walk($preprocessed-data, ())

    (: sometimes we have structural variance on chapter level - in these cases
    tei:app is the root element of $xml, not tei:div. for the HTML serialization
    of the structural variance to function properly we have to preserve the
    tei:app :)
    let $content :=
        if($xml[self::tei:app]) then
            element tei:app {
                $xml/@*,
                attribute id {generate-id($xml)},
                $intermediate-format
            }
        else
            ($xml/@*,
            attribute id {generate-id($xml)},
            $intermediate-format)

    let $transformed :=
            element tei:div {
                $content,
                $editorial-notes
            }
    let $store := xmldb:store($dir, $filename, $transformed)

    return $intermediate-format
};
