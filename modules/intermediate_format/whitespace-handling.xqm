xquery version "3.0";

(:
 : Copyright (c) 2018 Uwe Sikora
 : Copyright (c) 2018–2019 Michelle Weidling
 : Copyright (c) 2020 Stefan Hynek
 :
 : This file is part of intermediate-format.
 :
 : intermediate-format is free software: you can redistribute it and/or modify
 : it under the terms of the GNU General Public License as published by
 : the Free Software Foundation, either version 3 of the License, or
 : (at your option) any later version.
 :
 : intermediate-format is distributed in the hope that it will be useful,
 : but WITHOUT ANY WARRANTY; without even the implied warranty of
 : MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 : GNU General Public License for more details.
 :
 : You should have received a copy of the GNU General Public License
 : along with intermediate-format.  If not, see <https://www.gnu.org/licenses/>.
 :)

(:~
 : WHITESPACE Module ("whitespace", "http://bdn.edition.de/intermediate_format/whitespace_handling")
 : *******************************************************************************************
 : This module contains the functions to handle different whitespace operations on text
 :
 : @version 1.0 (2018-01-02)
 : @status working
 : @author Uwe Sikora
 :)
module namespace whitespace="http://bdn-edition.de/intermediate_format/whitespace_handling";

declare default element namespace "http://www.tei-c.org/ns/1.0";


(:############################# Modules Functions #############################:)

(:~
 : whitespace:text()
 : This function handles whitespace in defined text() nodes
 :
 : @param $text the text-node to be converted
 : @param $escape-char a optional escape-character replacing all whitespace characters
 : @return text()* representing the escaped text()
 :
 : @version 2.0 (2018-01-30)
 : @status working
 : @author Uwe Sikora
 :)
declare function whitespace:text
    ( $text as text()*, $escape-char as xs:string? ) as text()* {

    let $whitespace-node := $text[matches(., "[\n\r\t]") and normalize-space(.) = ""]

    return
        if (not($whitespace-node)) then (

            if ($escape-char) then (
                whitespace:escape-text($text, "@")
            ) else ( whitespace:escape-text($text, " ") )

        )
        else ()

};


(:~
 : whitespace:escape-text()
 : This function replaces whitespaces in a text()
 : with a defined preservation character
 :
 : @param $text the text-node to be converted
 : @param $escape the escape-character replacing all whitespace characters
 : @return text()* representing the escaped text()
 :
 : @version 2.0 (2018-01-30)
 : @status working
 : @author Uwe Sikora
 :)
declare function whitespace:escape-text
    ($text as text()*, $escape as xs:string) as text()* {

    text {replace($text, '[\s]+', $escape)}
};
