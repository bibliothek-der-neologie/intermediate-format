# [1.3.0](https://gitlab.gwdg.de/bibliothek-der-neologie/intermediate-format/compare/v1.2.0...v1.3.0) (2021-04-12)


### Bug Fixes

* **ifweb:** register library modules ([ea33a54](https://gitlab.gwdg.de/bibliothek-der-neologie/intermediate-format/commit/ea33a547dd1c8228dc17d3d543d56700a0b19d19)), closes [#5](https://gitlab.gwdg.de/bibliothek-der-neologie/intermediate-format/issues/5)
* **semantic-release:** change release branch from `production` to master ([9fb57d9](https://gitlab.gwdg.de/bibliothek-der-neologie/intermediate-format/commit/9fb57d97659502a4597fa2f05060f5039b5ccd9c))
* **whitespaces:** do not purge single spaces ([4aade28](https://gitlab.gwdg.de/bibliothek-der-neologie/intermediate-format/commit/4aade28993c597c23aa1bdf59630001f1e5a3d1e)), closes [#3](https://gitlab.gwdg.de/bibliothek-der-neologie/intermediate-format/issues/3)


### Features

* **semantic-release:** provide helper scripts for semantic-release ([dbf8b19](https://gitlab.gwdg.de/bibliothek-der-neologie/intermediate-format/commit/dbf8b1940a88499f1156380b0c562e35bc309444))

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## v1.2 - 2019-06-28
## Added
- added mechanism for spaced out text

## v1.1.1 - 2019-02-27
### Fixed
- fix bug in tei:row handling
